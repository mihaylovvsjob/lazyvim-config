return {
  {
    "folke/noice.nvim",
    opts = function(_, opts)
      table.insert(opts.routes, {
        filter = {
          event = "notify",
          find = "No information available",
        },
        opts = { skip = true },
      })

      opts.presets.lsp_doc_border = true
    end,
  },
  {
    "rcarriga/nvim-notify",
    opts = {
      timeout = 10000,
    },
  },
  {
    "shellRaining/hlchunk.nvim",
    event = { "UIEnter" },
    config = function()
      require("hlchunk").setup({
        indent = {
          chars = { ">", "•" },
          enable = true,
          use_treesitter = true,
        },
        blank = {
          enable = false,
          use_treesitter = true,
        },
        line_num = {
          enable = true,
          use_treesitter = true,
          style = { "#d5c4a1" },
        },
        chunk = {
          enable = true,
          use_treesitter = true,
          style = { "#d65d0e" },
        },
      })
    end,
  },
  -- {
  --   "shellRaining/hlchunk.nvim",
  --   event = { "UIEnter" },
  --   config = function()
  --     require("hlchunk").setup({
  --       line_num = {
  --         use_treesitter = true,
  --         enable = true,
  --         style = "#806d9c",
  --       },
  --       chunk = {
  --         enable = true,
  --         use_treesitter = true,
  --         error_sign = true,
  --         chars = {
  --           horizontal_line = "─",
  --           vertical_line = "│",
  --           left_top = "╭",
  --           left_bottom = "╰",
  --           right_arrow = ">",
  --         },
  --         style = {
  --           "#806d9c",
  --           "#c10b40",
  --         },
  --       },
  --     })
  --   end,
  -- },
}
