return {
  "stevearc/conform.nvim",
  opts = {},
  config = function()
    require("conform").setup({
      formatters_by_ft = {
        lua = { "stylua" },
        python = { "black", "isort" },
        java = { "clangd", "google-java-format" },
        markdown = { "markdownlint" },
        yaml = { "yamlfmt" },
        dart = { "dart_format" },
        sql = { "pg_format" },
      },
    })
  end,
}
